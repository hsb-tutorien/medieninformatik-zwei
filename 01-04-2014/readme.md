# 01. April 2014

Im Rahmen dieses Tutoriums wurde die Frage gestellt, wie das Zusammenspiel zwischen HTML und CSS zu sehen ist. Die in dem Verzeichnis `src` liegenden Dateien repräsentieren das Projekt, welches wir gemeinsam innerhalb des Tutoriums entwickelt haben - eine Website für ein rudimentäres Kochbuch.

## Zusammenfassung

### HTML

Das HTML repräsentiert die Strukturierung der jeweiligen Website / der jeweiligen Benutzungsoberfläche.

### CSS

Das CSS beschreibt, wie die in HTML definierte Struktur, visuell gestaltet werden soll.

#### Referenzierung

CSS-Dateien werden im `<head>` der jeweiligen HTML-Datei mittels `<link rel="stylesheet" href="pfad/zu/css/datei.css">` referenziert.

#### Selektoren

Die allgemeine Syntax eines CSS-Selektors:

    <name-des-selectors> {
        <style-eigenschaft>: <style-wert>
    }

Es existiert die Unterscheidung zwischen IDs - also eindeutig identifizierte Elemente (dürfen nur ein einziges Mal in der gesamten HTML-Datei vorhanden sein)

    #id-des-elements {
        background:red;
        /* ... */
    }

und Klassen (dürfen mehrfach in der HTML-Datei verwendet werden und können als Generalisierung / Zusammenfassung von Styles gesehen werden):

    .name-der-klasse {
        font-weight:bold;
    }

Beispiel:

    <!-- Die HTML-Deklarationen (index.html) -->

    <section id="rezept-sushi" class="rezept">Foo</section>
    <section id="rezept-pizza" class="rezept">Foo</section>

    /* Die jeweiligen CSS-Selektoren (styles/main.css) */

    .rezept {
        background:red;
    }

    #rezept-sushi {
        border:1px solid rgb(255, 0, 0);
    }

Die beiden Rezepte haben einen roten Hintergrund, wobei das "Sushi-Rezept" zusätzlich einen roten Rahmen besitzt.

#### Best practices

- CSS-Dateien immer in eigene Dateien auslagern (keine `<style>` Tags innerhalb der HTML-Datei verwenden).

- Keine CSS-Selektoren schreiben, die sich auf einzelne HTML-Elemente beziehen, sondern diese lieber mittels IDs bzw. Klassen _semantisieren_ (siehe oben)