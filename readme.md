# Medieninformatik 2 - Tutorium

Dies ist das "offizielle" Repository des Tutoriums "Medieninformatik 2" im "Int. Studiengang Medieninformatik B.Sc." an der Hochschule Bremen. Hier findet Ihr den Code, welchen wir gemeinsam in den jeweiligen Tutorien produzieren (eigenes Verzeichnis mit Timestamp), und zudem noch interessante Links, die Euch bei der Arbeit an den Projekten unterstützen könnten.

Wenn Ihr Fragen habt und wir uns gerade nicht in einem Tutorium sehen, so könnt Ihr mir gerne eine [E-Mail](mailto:akoenig@posteo.de) schreiben oder hier in dem Repository ein Ticket erstellen.

## Installation

Das Repository könnt Ihr wie folgt klonen:

    $ git clone https://bitbucket.org/hsb-tutorien/medieninformatik-zwei

Wenn es hier Updates gibt und Ihr diese auch bei Euch lokal haben möchtet, so könnt Ihr es einfach "pullen":

    $ git pull origin master

## Links

In diesem [Dokument](https://bitbucket.org/hsb-tutorien/medieninformatik-zwei/src/HEAD/bookmarks.md?at=master) findet Ihr Bookmarks, welche Euch bei der Arbeit in dem Projekt unterstützen könnten.
