# Bookmarks

## Allgemein

- [Mozilla Developer Network](https://developer.mozilla.org/): Referenz zu den Themen HTML, CSS und JavaScript.

## CSS

- [normalize.css](https://github.com/necolas/normalize.css/) - Der Browser bringt ja einige Standardstyles mit. Um diese zu "normalisieren" ist diese CSS-Datei empfehlenswert.

## Abstraktionsbibliotheken (Canvas)

Im Folgenden findet Ihr Abstraktionsbibliotheken, die Euch bei der Arbeit mit dem HTML5 Canvas unterstützen. Mit diesen habe ich bereits gearbeitet und kann sie somit empfehlen.

- [Three.js](http://threejs.org/) - Sehr gelungene Bibliothek, die ich Euch bei der Umsetzung von 3D-Visualisierungen auf der HTML5 Canvas unterstützt.
- [Paper.js](http://paperjs.org/) - Wenn 3D in Eurem Projekt keine Rolle spielt, so ist Paper.js eine gute Alternative, wenn es um Operationen im 2D-Raum geht.

## Physics Engines

Wenn Ihr Effekte umsetzen wollt, die der realen Welt nahe kommen, so sind diese "Physics Engines" zu empfehlen:

- [Box 2D](http://box2d-js.sourceforge.net/) - Diese habe ich einmal in einem Projekt verwendet. Handling ist gewöhnungsbedürftig.
- [PhysicsJS](http://wellcaffeinated.net/PhysicsJS/)

## Hacking

Links zu experimentellen Features:

[HTML5 Gamepad API](http://www.html5rocks.com/en/tutorials/doodles/gamepad/?redirect_from_locale=de) - Eine Erläuterung zu der Gamepad API, die es ermöglicht Gamepads in Euren Projekten zu verwenden (eine Wiimote-Anbindung habe ich allerdings nicht gefunden ;) ).