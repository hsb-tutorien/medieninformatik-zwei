# 08. April 2014

## Ballgame

Im Ballgame-Verzeichnis befindet sich eine kleine Anwendung auf Basis des HTML5 Canvas. Ein Ball, der mittels Cursor-Tasten bewegt werden kann. Klingt erst einmal nicht spektakulär. Das Beispiel zeigt aber wesentliche Kernkonzepte. Zum einen wird das OOP-Paradigma auf JavaScript übertragen und gezeigt, wie sich klassenähnliche Strukturen erstellen lassen; zum anderen zeigt das Beispiel auch, wie das Rendern auf der Canvas durchgeführt wird. Dabei werden zudem grundlegende Best Practices, wie Namespacing, etc. visualisiert.

Ich kann nur dazu ermutigen mit dem Beispiel ein wenig zu experimentieren (mehrere Bälle hinzufügen, Farben ändern), um so ein Gefühl für das Arbeiten mit der HTML5 Canvas zu gewinnen. Wenn diese wesentlichen Konzepte durchdrungen wurden, so bietet es sich an, einen Blick auf Abstraktionsbibliotheken zu werfen, welche die Arbeit mit der HTML5 Canvas ein wenig vereinfachen. Einige dieser Bibliotheken sind in dem Dokument [bookmarks.md](https://bitbucket.org/hsb-tutorien/medieninformatik-zwei/src/HEAD/bookmarks.md?at=master) hinterlegt.