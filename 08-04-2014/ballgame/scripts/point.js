//
// Erstellen eines Namespaces (siehe "app.js" für Erläuterung) oder Verwendung
// des bestehenden Namespaces.
//
var Ballgame = Ballgame || {};

//
// Initialisierung der "Point-Klasse" (IIFE; siehe "app.js" für Erläuterung).
//
(function init () {

    'use strict';

    /**
     * Konstruktor-Funktion der "Point-Klasse"
     *
     * @param {number} radius Der Radius des Punktes
     * @param {string} color Die Farbe des Punktes.
     * @param {number} startX Der Startwert auf der X-Achse (optional; default = 20px)
     * @param {number} startY Der Startwert auf der Y-Achse (optional; default = 20px)
     *
     */
    function Point (radius, color, startX, startY) {

        this.radius = radius;
        this.color = color;

        this.x = startX || 20; // Wenn startX nicht gesetzt, so ist der Startwert 20 Pixel.
        this.y = startY || 20;

        //
        // Deklaration eines Event-Listeners, der auf Ereignisse der Tastatur reagiert.
        // Wenn also das Event 'keydown' eintritt, so wird die "Methode" `handleMove` ausgeführt.
        // 
        // Anmerkung: Das `.bind(this)` gibt an in welchem Kontext diese Methode ausgeführt wird.
        // siehe https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/bind
        //
        document.addEventListener('keydown', this.handleMove.bind(this));
    }

    /**
     * Reagiert auf Tastatur-Ereignisse
     *
     * @param  {object} event Informationen zu dem Ereignis
     *
     */
    Point.prototype.handleMove = function handleMove (event) {

        //
        // Schrittweite für die Bewegung (in Pixel) pro Tastendruck.
        //
        var step = 2;

        switch (event.keyCode) {

            // Links
            case 37:
                this.x = this.x - step;
            break;

            // Rechts
            case 39:
                this.x = this.x + step;
            break;
        
            // Hoch
            case 38:
                this.y = this.y - step;
            break;

            // Runter
            case 40:
                this.y = this.y + step;
            break;
        }
    }

    //
    // Dem Namespace die "Point-Klasse" hinzufügen.
    //
    Ballgame.Point = Point;
})();