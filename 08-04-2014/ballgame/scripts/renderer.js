//
// Erstellen eines Namespaces (siehe "app.js" für Erläuterung) oder Verwendung
// des bestehenden Namespaces.
//
var Ballgame = Ballgame || {};

//
// Initialisierung der "Point-Klasse" (IIFE; siehe "app.js" für Erläuterung).
//
(function init () {

    'use strict';

    /**
     * Die "Renderer-Klasse".
     *
     * @param {DOMElement} $canvas Das Canvas DOM-Element.
     * @param {number} fps The FPS, die erreicht werden sollen (optional; default = 30)
     *
     */
    function Renderer ($canvas, fps) {
        this.fps = fps || 30;
        this.context = $canvas.getContext('2d');
        this.height = $canvas.getAttribute('height');
        this.width = $canvas.getAttribute('width');

        this.points = [];

        this.intervalId = 0;
    }

    /**
     * Renderer starten
     *
     */
    Renderer.prototype.start = function start () {
        this.intervalId = setInterval(this.draw.bind(this), (1000 / this.fps));
    };

    /**
     * Renderer anhalten
     *
     */
    Renderer.prototype.stop = function stop () {
        clearInterval(this.intervalId);

        this.intervalId = null;
    };

    /**
     * Einen Punkt-Objekt dem Renderer hinzufügen
     *
     * @param {Ballgame.Point} point
     *
     */
    Renderer.prototype.addPoint = function addPoint (point) {
        this.points.push(point);
    };

    /**
     * Die eigentliche Zeichenmethode. Löscht die Canvas und zeichnet jeden
     * Punkt neu. Diese Methode wird je nach FPS-Wert sehr häufig ausgeführt.
     *
     * @return {[type]} [description]
     *
     */
    Renderer.prototype.draw = function draw () {
        var len = this.points.length;
        var i = 0;
        var point;

        this.context.clearRect(0, 0, this.width, this.height);

        for (i; i < len; i = i + 1) {
            point = this.points[i];

            // Den Punkt neu zeichnen.
            this.context.beginPath();

            // Methoden-Parameter: x, y, radius, Start-Winkel, End-Winkel
            this.context.arc(point.x, point.y, point.radius, 0, Math.PI * 2); 
            this.context.closePath();

            this.context.fillStyle = point.color;

            this.context.fill();
        }
    };

    //
    // Dem Namespace die "Renderer-Klasse" hinzufügen.
    //
    Ballgame.Renderer = Renderer;
})();