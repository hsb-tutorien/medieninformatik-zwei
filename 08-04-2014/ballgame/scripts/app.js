//
// Erstellen eines Namespaces (siehe unten)
//
var Ballgame = Ballgame || {};

//
// Diese folgende Funktion verhält sich wie die "main"-Methode, die Ihr aus der Java-Welt kennt.
// Sie ist dabei als eine sogenannte "Immediately-invoked function expression" (IIFE) gestaltet.
// Eine Funktion, die definiert und im Anschluss direkt ausgeführt wird.
// 
// Längere Schreibweise wäre:
// 
//     var main = function main () {...};
//     main();
// 
// Dieses Pattern wird verwendet, um den sogenannten globalen Variablenscope nicht zu
// "beschmutzen". Wie ich ja im letzten Tutorium angemerkt hatte, ist der Gültigkeitsbereich
// von Variablen auf Funktionen bezogen. Existiert keine Funktion, wie etwa:
// 
//     var foo = 'bar'
//     
// statt
// 
//    function test () {
//        var foo = 'bar'
//    }
// 
// so habt Ihr oben eine globale Variable erstellt. Das wird dann problematisch, wenn Ihr in Euren
// Anwendungen weitere Bibliotheken verwendet. Beispiel:
// 
//     Nehmen wir an, dass Ihr zusätzlich `jQuery` verwendet.
//     
//     Wenn Ihr also nun aus Versehen eine globale Variable var jQuery = 'bla'; erstellt,
//     so habt Ihr die Bibliothek überschrieben.
// 
// Das ist auch der Grund, warum wir oben explizit eine globale Variable "Ballgame" erstellt haben.
// Innerhalb dieser Variablen können wir uns "austoben" (Namespacing). Diese Art der Deklaration ist vergleichbar
// mit Packages in Java.
// 

(function main () {

    'use strict';

    //
    // Anzahl der FPS (frames per second), die wir für das Rendern erreichen wollen.
    //
    var fps = 30;

    //
    // Beziehen des Canvas-Elements aus dem DOM.
    // 
    var $canvas = document.querySelector('#ballgame-canvas');

    //
    // Instanziierung unseres eigenen Render-Objektes (siehe "renderer.js").
    //
    var renderer = new Ballgame.Renderer($canvas);

    //
    // Instanziierung eines Point-Objektes (siehe "point.js")
    //
    var point = new Ballgame.Point(10, '#81b71a');

    //
    // Unserem Renderer den Punkt hinzufügen.
    //
    renderer.addPoint(point);

    //
    // Starten des Renderers.
    // 
    renderer.start();
})();